# PhpInsights Block

## CONTENTS OF THIS FILE

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Using the module
  * Author

## INTRODUCTION

PhpInsights Block is a Drupal 8 module that allows you to pull content. 
An easy-to-use API Wrapper for Google PageSpeed Insights -
`https://developers.google.com/speed/docs/insights/v2/reference
/pagespeedapi/runpagespeed` 
The JSON response is mapped to objects for an headache-free usage.

## REQUIREMENTS

None.

## INSTALLATION

1. Get an api key from the google developer console for 
   Page Speed Insights - `https://console.developers.google.com
   /apis/api/pagespeedonline-json.googleapis.com/overview`
2. Install module as usual via Drupal UI, Drush or Composer.
3. Go to "Extend" and enable the PhpInsights Block module.
4. Have fun with this module.

## CONFIGURATION

In order to get access to the Google PageSpeed Insights API 
you need a unique API key. Go to
`https://console.developers.google.com/apis
/api/pagespeedonline-json.googleapis.com/overview` and 
register your app. After that, go to
"Configuration" -> "WEB SERVICES" -> "PHPInsights Block" and 
enter your API key and the username or user ID of the project's 
owner.

## USING THE MODULE

1. Create a basic page.
2. Add a PhpInsights Block to this page by going to 
"Structure" -> "Block layout" and then click on the Place 
block button.

### AUTHOR

Ilcho Vuchkov 
Website: (https://ilchovuchkov.com)  
Drupal: (https://www.drupal.org/u/ilchovuchkov)  
