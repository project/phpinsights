<?php

namespace Drupal\phpinsights\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Contains \Drupal\phpinsights\Plugin\Block\PhpInsightsBlock.
 */

/**
 * Provides a 'PhpInsights Block' Block.
 *
 * @Block(
 *   id = "phpinsights",
 *   admin_label = @Translation("PhpInsights Block"),
 *   category = @Translation("Integrations"),
 * )
 */
class PhpInsightsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The backend cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  protected $apiKey;
  protected $userID;
  protected $newTab;

  /**
   * The constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The backend cache.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory
      ->get('phpinsights.settings');

    $this->apiKey = $config->get('api_key');
    $this->userID = $config->get('user_id');
    $this->newTab = $config->get('new_tab');

    $is_api_key_set = (isset($this->apiKey) && !empty($this->apiKey));
    $is_user_id_set = (isset($this->userID) && !empty($this->userID));

    // API key and User ID are set - show Google PageSpeed Insights page.
    if ($is_api_key_set && $is_user_id_set) {
      return [
        '#theme' => 'phpinsights',
        '#projects' => $this->projects(),
        '#tags' => $this->tags(),
        '#new_tab' => $this->newTab == 0 ? 'target=_self' : 'target=_blank',
        '#cache' => [
          'max-age' => 0,
        ],
        '#attached' => [
          'library' => [
            'phpinsights/phpinsights',
          ],
        ],
      ];
    }
    // Show error if required values are missing.
    else {
      return [
        '#markup' => 'You must set an API key and the username in the module settings. <a href="/admin/config/services/phpinsights">Click here</a> to go the module settings.',
        '#cache' => [
          'max-age' => 0,
        ],
        '#attached' => [
          'library' => [
            'phpinsights/phpinsights',
          ],
        ],
      ];
    }
  }

  /**
   * Returns array with all projects.
   */
  private function projects() {
    // Get projects from the cache.
    $cache = $this->cacheBackend;
    $cached_data = $cache->get('phpinsights:projects');
    if ($cached_data) {
      return unserialize($cached_data->data);
    }

    $projects = $this->fetchProjects();

    // Store data to the cache.
    $cache->set(
      'phpinsights:projects',
      serialize($projects),
      time() + 86400
    );

    return $projects;
  }

  /**
   * Get Behance projects from API endpoint.
   */
  private function fetchProjects() {
    $i = 1;
    $loop_through = TRUE;
    $all_projects = [];

    $client = new Client();

    // Loop while you get not empty JSON response.
    while ($loop_through) {
      $projects_json = [];
      try {
        // @TODO Replace the Uri.
        $response = $client->get('http://api.behance.net/v2/users/' .
          $this->userID . '/projects?api_key=' . $this->apiKey . '&per_page=24&page=' . $i);
        $response_code = $response->getStatusCode();
        $projects_json_page = $response->getBody();
        $projects_json = json_decode($projects_json_page, TRUE);
      }
      catch (ClientException $e) {
        $response = $e->getResponse();
        $response_code = $response->getStatusCode();
        \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('phpinsights'), $e), fn() => watchdog_exception('phpinsights', $e));
      }

      if ($response_code == 200) {
        if (isset($projects_json['projects']) && count($projects_json['projects']) > 0) {
          foreach ($projects_json['projects'] as $projects) {
            $all_projects[] = $projects;
          }
          $i++;
        }
        else {
          $loop_through = FALSE;
        }
      }
      else {
        $loop_through = FALSE;
      }

    }

    return $all_projects ? $all_projects : [];
  }

  /**
   * Returns all PhpInsights tags in array.
   */
  private function tags() {
    // Get tags from the cache.
    $cache = $this->cacheBackend;
    $cached_data = $cache->get('phpinsights:tags');
    if ($cached_data) {
      return unserialize($cached_data->data);
    }

    $tags = $this->fetchTags();

    // Store data to the cache.
    $cache->set(
      'phpinsights:tags',
      serialize($tags),
      time() + 86400
    );

    return $tags;
  }

  /**
   * Get PhpInsights field names (tags) and store them in JSON file.
   */
  private function fetchTags() {
    // Get response from endpoint and save it.
    $client = new Client();

    try {
      // @TODO Replace with the correct Url.
      $response = $client->get('http://api.behance.net/v2/fields?api_key='
        . $this->apiKey);
      $behance_fields_json = $response->getBody();
      $behance_fields = json_decode($behance_fields_json, TRUE);

      $tags = [];
      foreach ($behance_fields['fields'] as $behance_field) {
        $tags[$behance_field['name']] = $behance_field['id'];
      }
      return $tags;
    }
    catch (ClientException $e) {
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('phpinsights'), $e), fn() => watchdog_exception('phpinsights', $e));
      return [];
    }
  }

}
